# General
- Fix post editing causing hover replies to float forever
- Make inline editing user server endpoint which checks userhash, not password
- Add separate column storing whether an image is a spoiler or deleted
- Put "last edited timestamp" column in posts tables
- Fix "nothing to edit" message when trying to edit ancient post
- Site settings table
- One posts table instead of one table per board
- Improve ban screen
- [rawhtml]
- How do post updates work with previews?
- #unread is broken on mature threads
- Add flag preview to QR
- Themes page for users to see screenshots of all themes at once
  - maybe just iframes of the actual site showing the themes. (Can you use css to zoom an iframe?)
- separate out tinyboard code and all instance files (config files, cache, board HTML and image files) so deploys can finally be as easy as rsyncing the core/ directory over.
- ban signing, move signing. Add column for moderator names to mod table. Tripcode output should be included, not tripcode passwords.
- Use dropdown menu for post checkboxes
  - add [#cyclic] option
- Add config option to require spoilered threads to have a subject
- Timed deletion board. (Threads that haven't been bumped in a set amount of time are pushed off?)
- Make moving a thread to a board it already exists on cause an error
- Phase out all inline javascript and javascript: links
- Thread tags and board thread filter toggle buttons
- Dropdown for remembered names+trips? https://helgesverre.com/blog/textbox-dropdown-html5/

# Development improvements
- make deploying changes not suck. Deploys are manual and dumb.
  - Make sure not to overwrite instance-config.php when sending changes over! lol fml. Was kinda thinking of using Docker.
- More selenium tests

# Moderation
- Allow disabling the board sorting of posts on IP page
- Search by name/trip/filename pagination
- Page showing posts by new users
- Mods having access to the blotter
- Better bot/spam mitigation
- Let (mod?) editing (un)spoiler a post. (need to generate/remove thumbnail)
- Mod controls on IP page? need progress indicator
- [D++] is slow
- view deleted posts
- Allow mods to revert post changes/deletion
- Allow mods to see (with [deleted] in the post header bright red maybe)
- Remove [D+] button, rename [D++] to [D+]
- "Delete all posts by IP on one board" is not useful enough to exist and take screen space on every post.
- Maybe the delete buttons should be moved into a dropdown menu on posts
- Put some side channel for mods/admins to put scripts with posts besides just having the script in the post html

# System
- Fix cache consistency
- Make reply queuing a toggleable setting that defaults to off
- Image filename is renamed so old URL stops working
- Persist deleted threads and posts
- Include threads that were recently pushed off of the board
- Use Content-Security-Policy headers
- Separate name cookie on anon

# /all/
- /all/ overboard with tag filters and sorting order options
- Use JSON API
- Static page that uses ajax and react
