<?php
//error_reporting(E_ALL);
// This file is for Ponychan's JSON API. Its syntax will be somewhat similar to 4chan's
// at https://github.com/4chan/4chan-API but rather than static JSON files, this will be dynamic
// written by Ly (with the exception of listWatched, which was written by Zeke)

require_once 'inc/functions.php';
require_once 'inc/anti-bot.php';
require_once 'inc/database.php';
require_once 'inc/mod.php';
class apireq {
	static $req_types		=	array(
		"banner"			=>	"getBanner",
		"boards"			=>	"listBoards",
		"threads"			=>	"listThreads",
		"thread"			=>	"listPosts",
		"catalog"			=>	"listCatalog",
		"watcher"			=>	"listWatched"
	);

	private $req_type 		= 	"";
	private $board 			=	"";
	private $page 			=	0;
	private $thread			=	0;
	private $per_page 		=	0;
	private $page_count		=	0;
	private $banner			=	array();

	const 	max_thr_p_page	=	10;
	const 	max_page_count	=	10;
	const 	max_post_p_thr	=	150;

	public function __construct() {
		if (isset($_GET["req"]) && strlen($_GET["req"]) > 0 && array_key_exists($_GET["req"], self::$req_types))
			$this->req_type = $_GET["req"];
		else
			$this->err("Unknown request type. Allowed request types: ".implode(", ", array_keys(self::$req_types)));

		if (isset($_GET["board"]) && strlen($_GET["board"]) > 0)
			$this->board = addslashes($_GET["board"]);

		if (isset($_GET["thread"]) && strlen($_GET["thread"]) > 0 && is_numeric($_GET["thread"]))
			$this->thread = (int)$_GET["thread"];

		if (isset($_GET["page"]) && strlen($_GET["page"]) > 0 && is_numeric($_GET["page"]))
			$this->page = ((int)$_GET["page"] > 0 ? (int)$_GET["page"] - 1 : 0);

		global $config, $mod;
		if(isset($_GET["board"]) && in_array($_GET["board"], $config['modboards']) && !hasPermission('modboards'))
			$this->err("Permission denied");

		$this->per_page = (int)min((int)$config["threads_per_page"], self::max_thr_p_page);
		$this->page_count = (int)min((int)$config["max_pages"], self::max_page_count);

		$this->{self::$req_types[$this->req_type]}();
	}

	private function getBanner() {
		global $config;
		$banners = array();
		$domain = $_SERVER['HTTP_HOST'];
		$prefix = "pchan/";
		if($domain == "pchan.co")
			$banners = $config['pchan_banners'];
		else {
			$banners = $config['ponychan_banners'];
			$prefix = "ponychan/";
		}

		$banner = array(
			"banner" => "",
			"width" => 0,
			"height" => 0
		);
		if(count($banners) > 0) {
			$rand = array_rand($banners, 1);
			$banner = $banners[$rand];
		}
		$this->success($banner);
	}

	private function listCatalog() {
		$errors = array();

		if (strlen($this->board) === 0)
			$errors[] = "Board not specified.";

		if (count($errors))
			$this->err($errors);

		global $config;

		$board = "posts_".$this->board;

		$limit = $this->per_page * $this->page_count;

		$data = query("SELECT main.*, INET6_NTOA(main.ip_data) as ip_data, (SELECT COUNT(*) FROM `$board` rc WHERE rc.`thread` = main.`id`) as replyCount, (SELECT COUNT(*) FROM `$board` ic WHERE ic.`thread` = main.`id` AND `file` != 'deleted' AND `file` IS NOT NULL) as imageCount FROM `$board` main ".
						"WHERE `thread` IS NULL ".
						"ORDER BY `sticky` DESC, `bump` DESC ".
						"LIMIT $limit");

		if (!$data)
			$this->err("Invalid board or database error.");

		$results = array();
		$dlFor = array();

		while ($result = $data->fetch(PDO::FETCH_OBJ)) {
			$results[] = $result;
			$dlFor[] = $result->id;
		}

	  	$data = query($this->generateUnionSQL($dlFor, $board, 1));

		if (!$data)
			$this->err("Invalid board or database error.");

		$endResults = array();

		$repliesTo = array();

		while ($result = $data->fetch(PDO::FETCH_OBJ))
			$repliesTo[(int)$result->thread][] = $result;

		foreach ($repliesTo as $key => $val)
			$repliesTo[$key] = array_reverse($val);

		$pageNum = 0;

		foreach ($results as $thread) {
			$thrPosts = array();

			$imagesInPosts = 0;

			if (isset($repliesTo[(int)$thread->id])) {
				foreach ($repliesTo[(int)$thread->id] as $reply) {
					$thrPosts[] = $this->getPost($reply);
					if (isset($thrPosts[count($thrPosts)-1]->filename))
						$imagesInPosts++;
				}
			}

			if (!isset($endResults[$pageNum]))
				$endResults[$pageNum] = array(
					"page"				=>	$pageNum+1,
					"threads"			=>	array()
				);

			$OP = $this->getOP($thread);

			$endResults[$pageNum]["threads"][] = array_merge(
				$OP,
				array(
					"last_replies"	=>	$thrPosts,
					"omitted_posts"	=>	$OP["replies"] - (isset($repliesTo[(int)$thread->id]) ? count($repliesTo[(int)$thread->id]) : 0),
					"omitted_images"=>	$OP["images"] - $imagesInPosts
				)
			);

			if (count($endResults[$pageNum]["threads"]) >= $this->per_page)
				$pageNum++;
		}

		$this->success($endResults);
	}

	private function listBoards() {
		$data = query("SELECT * FROM `boards` WHERE `uri` <> 'castle' ");

		if (!$data)
			$this->err("Couldn't fetch boards.");

		$boards = array();

		global $config;

		while ($board = $data->fetch(PDO::FETCH_OBJ)) {
			$boards[] = array(
				"board"					=>	$board->uri,
				"title"					=>	$board->title,
				"per_page"				=>	$config["threads_per_page"],
				"pages"					=>	$config["max_pages"],
				"max_filesize"			=>	$config["max_filesize"],
				"max_webm_filesize"		=>	$config["max_video_filesize"],
				"max_comment_chars"		=>	$config["max_body"],
				"max_webm_duration"		=>	$config["max_video_length"],
				"bump_limit"			=>	$config["reply_limit"],
				"image_limit"			=>	$config["image_hard_limit"],
				"cooldowns"				=>	array(
					"replies"			=>	$config["flood_time"],
					"images"			=>	$config["flood_time"],
					"threads"			=>	$config["flood_time_op"][0][1]
				),
				"meta_description"		=>	$board->subtitle,
			);
		}

		$this->success(array("boards" => $boards));
	}

	private function listThreads() {
		$errors = array();

		if (strlen($this->board) === 0)
			$errors[] = "Board not specified.";

		if (count($errors))
			$this->err($errors);

		global $config;

		$board = "posts_".$this->board;

		$offset = $config["threads_per_page"] * $this->page;

		$data = query("SELECT main.*, INET6_NTOA(ip_data) as ip_data, (SELECT COUNT(*) FROM `$board` rc WHERE rc.`thread` = main.`id`) as replyCount, (SELECT COUNT(*) FROM `$board` ic WHERE ic.`thread` = main.`id` AND `file` != 'deleted' AND `file` IS NOT NULL) as imageCount FROM `$board` main ".
						"WHERE `thread` IS NULL ".
						"ORDER BY `sticky` DESC, `bump` DESC ".
						"LIMIT ".$config["threads_per_page"]." OFFSET $offset");

		if (!$data)
			$this->err("Invalid board or database error.");

		$results = array();
		$dlFor = array();

		while ($result = $data->fetch(PDO::FETCH_OBJ)) {
			$results[] = $result;
			$dlFor[] = $result->id;
		}

		$data = query($this->generateUnionSQL($dlFor, $board, 3));

		if (!$data)
			$this->err("Invalid board or database error.");

		$endResults = array();

		$repliesTo = array();

		while ($result = $data->fetch(PDO::FETCH_OBJ))
			$repliesTo[(int)$result->thread][] = $result;

		foreach ($repliesTo as $key => $val)
			$repliesTo[$key] = array_reverse($val);

		foreach ($results as $thread) {
			$thrPosts = array();

			$imagesInPosts = 0;

			if (isset($repliesTo[(int)$thread->id])) {
				foreach ($repliesTo[(int)$thread->id] as $reply) {
					$thrPosts[] = $this->getPost($reply);
					if (isset($thrPosts[count($thrPosts)-1]->filename))
						$imagesInPosts++;
				}
			}

			$OP = $this->getOP($thread);

			$endResults[] = array(
				"posts"		=>	array_merge(array(
					array_merge(
						$OP,
						array(
							"omitted_posts"	=>	$OP["replies"] - (isset($repliesTo[(int)$thread->id]) ? count($repliesTo[(int)$thread->id]) : 0),
							"ommited_images"=>	$OP["images"] - $imagesInPosts
						)
					)
				), $thrPosts)
			);
		}

		$this->success(array("threads" => $endResults));
	}

	private function listPosts() {
		$errors = array();

		if (strlen($this->board) === 0)
			$errors[] = "Board not specified.";

		if ($this->thread === 0)
			$errors[] = "Thread not specified.";

		if (count($errors))
			$this->err($errors);

		global $config;

		$board = "posts_".$this->board;

		$data = query("(SELECT main.*, INET6_NTOA(ip_data) AS ip_data, (SELECT COUNT(*) FROM `$board` rc WHERE rc.`thread` = main.`id`) AS replyCount, (SELECT COUNT(*) FROM `$board` ic WHERE ic.`thread` = main.`id` AND `file` != 'deleted' AND `file` IS NOT NULL) AS imageCount FROM `$board` main ".
						"WHERE `id` = '{$this->thread}' ".
						"ORDER BY `id` ASC) ".
						"UNION ALL ".
						"(SELECT *, INET6_NTOA(ip_data), '0' AS replyCount, '0' AS imageCount FROM `$board` ".
						"WHERE `thread` = '{$this->thread}' ".
						"ORDER BY `ID` DESC ".
						"LIMIT ".self::max_post_p_thr.")");

		if (!$data)
			$this->err("Invalid thread, board or database error.");

		$results = array();

		while ($result = $data->fetch(PDO::FETCH_OBJ))
			$results[] = $result;

		$endResults = array();

		if (!count($results))
			$this->success(array("posts" => $endResults));

		$OP = $results[0];

		array_shift($results);
		$results = array_reverse($results);

		$endResults[] = $this->getOP($OP);

		foreach ($results as $result)
			$endResults[] = $this->getPost($result);

		$this->success(array("posts" => $endResults));
	}

	private function listWatched() {
		$watched = array();
		$mod_data = $this->getModWatched(); // get number of reports and appeals if we're logged in
		
		if(!isset($_COOKIE["watched_threads"])) {
			$this->success(array(
				"watched" => $watched,
				"mod" => $mod_data
			));
		}
		
		$json = array();
		$json = json_decode($_COOKIE["watched_threads"], true) or
			$this->err(array("Unable to decode watched threads cookie: " . json_last_error_msg()));

		$unique = array(); // e.g. array("uri" => array(1,2,3,...))

		foreach ($json as $key => $value) {
			$thread_board = explode(":", $key, 2);
			if (count($thread_board) < 2)
				$this->err(array("Invalid watched thread key: $key, must be board:threadid"));

			$board = $thread_board[0];
			$thread = $thread_board[1];

			if (!isset($unique[$board]))
				$unique[$board] = array();
			$unique[$board][] = $thread;
		}
		$th = array();
		foreach ($unique as $unique_board => $threads) {
			foreach ($threads as $t => $thread) {
				$query = prepare("SELECT `id`,`thread`,`time` FROM `posts_$unique_board` WHERE `thread` = :thread ORDER BY `id` DESC");
				$query->bindValue(":thread", $thread);
				$query->execute() or
					$this->err("Invalid board or database error.");

				$num_replies = $query->rowCount();

				$id = $thread;
				$time = 0;
				while($rep = $query->fetch(PDO::FETCH_ASSOC)) {
					if($rep["id"] > $id) {
						$id = $rep["id"];
						$time = $rep["time"];
					}
				}

				$watched_key = "$unique_board:$thread";
				$watched[$watched_key] = array(
					"id" => $id,
					// "thread" => $thread,
					"time" => $time,
					"replies" => $num_replies,
				);
			}
		}

		$this->success(array(
			"watched" => $watched,
			"mod" => $mod_data
		));
	}

	private function generateUnionSQL($parentIDs, $board, $limit) {
		if (count($parentIDs) === 0)
			return "";

		if (count($parentIDs) === 1)
			return "SELECT * FROM `$board` WHERE `thread` = '".$parentIDs[0]."' ORDER BY `id` DESC LIMIT $limit";

		$union = "";

		foreach ($parentIDs as $key => $ID) {
			$union .= "(SELECT *, INET6_NTOA(ip_data) as ip_data FROM `$board` WHERE `thread` = '".$ID."' ORDER BY `id` DESC LIMIT $limit)";
			if (count($parentIDs)-1 !== $key)
				$union .= " UNION ALL ";
		}

		return $union;
	}

	private function getOP($post) {
		return array_merge(array(
			"sticky"			=>	(int)$post->sticky,
			"closed"			=>	(int)$post->locked,
			"mature"			=>	(int)$post->mature,
			"anon"				=>	(int)$post->anon_thread,
			"replies"			=>	(int)$post->replyCount,
			"images"			=>	(int)$post->imageCount
		), $this->getPost($post));
	}

	private function getModWatched() {
		global $mod;
		$mod_data = array();
		if($mod) {
			$query = query("SELECT (SELECT COUNT(*) FROM `reports`) AS `num_reports`, (SELECT COUNT(*) FROM `ban_appeals`) AS `num_appeals`;");
			$mod_data = $query->fetch(PDO::FETCH_ASSOC);
		}
		return $mod_data;
	}

	private function getPost($post) {
		$body_cutoff = 500;
		$com = $post->body_nomarkup;
		if(in_array($this->req_type, array("catalog", "threads"))) {
			if(strlen($post->body_nomarkup) > $body_cutoff)
				$com = substr($post->body_nomarkup, 0, $body_cutoff)."...";
		}

		$postData = array(
			"no"				=>	(int)$post->id,
			"now"				=>	date("m/d/y(D)H:i:s"),
			"name"				=>	$post->name,
			"com"				=>	$post->body_nomarkup,
			"time"				=>	(int)$post->time,
			"resto"				=>	(int)$post->thread,
			"id"				=>	poster_id($post->ip_data, ($post->thread > 0)?$post->thread:$post->id)
		);
		if(isset($post->country_code) && isset($post->country_name) && $post->country_type != "blank") {
			$postData["board_flag"] = $post->country_code;
			$postData["flag_name"] = $post->country_name;
		}

		if ($post->email)
			$postData["email"] = $post->email;

		if ($post->capcode)
			$postData["capcode"] = $post->capcode;

		if ($post->subject)
			$postData["sub"] = $post->subject;

		if ($post->trip)
			$postData["trip"] = $post->trip;

		if ($post->thumb == "spoiler")
			$postData['spoiler'] = 1;

		return array_merge($postData, $this->getFileInfo($post));
	}

	private function getFileInfo($post) {
		if (!isset($post->filename) || !strlen($post->filename))
			return array();

		return array(
			"filename"			=>	$post->filename,
			"file"				=>	$post->file,
			"ext"				=>	substr($post->file, strrpos($post->file, ".")),
			"w"					=>	(int)$post->filewidth,
			"h"					=>	(int)$post->fileheight,
			"tn_w"				=>	(int)$post->thumbwidth,
			"tn_h"				=>	(int)$post->thumbheight,
			"md5"				=>	$post->filehash,
			"fsize"				=>	(int)$post->filesize,
		);
	}

	private function success($data) {
		header("Content-Type: application/json");
		utf8_clean_array($data);

		$encoded = json_encode($data, JSON_NUMERIC_CHECK);
		$error = json_last_error_msg();
		if($error != "No error") $this->err($error);

		header("Content-Length: " . strlen($encoded));
		die($encoded);
	}

	private function err($error) {
		header("Content-Type: application/json");
		die(json_encode(array(
			"status"		=>	"error",
			"messages"		=>	(is_array($error) ? $error : array($error))
		)));
	}
}

$request = new apireq();
