{% verbatim %}

window.SITE_DATA = {% endverbatim %}{{ SITE_DATA|raw }}{% verbatim %};

{% endverbatim %}{% if config.google_analytics %}{% verbatim %}

window._gaq = window._gaq || [];
_gaq.push(['_setAccount', '{% endverbatim %}{{ config.google_analytics|e('js') }}{% verbatim %}']);
{% endverbatim %}{% if config.google_analytics_domain %}{% verbatim %}
_gaq.push(['_setDomainName', '{% endverbatim %}{{ config.google_analytics_domain|e('js') }}{% verbatim %}']);
{% endverbatim %}{% endif %}{% if not config.google_analytics_domain %}{% verbatim %}
_gaq.push(['_setDomainName', 'none']);
{% endverbatim %}{% endif %}{% verbatim %}
_gaq.push(['_trackPageview']);
(function() {
  var ga = document.createElement('script');
  ga.type = 'text/javascript';
  ga.async = true;
  ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
  var s = document.getElementsByTagName('script')[0];
  s.parentNode.insertBefore(ga, s);
})();

{% endverbatim %}{% endif %}
