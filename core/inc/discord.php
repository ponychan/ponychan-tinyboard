<?php

if (realpath($_SERVER['SCRIPT_FILENAME']) == str_replace('\\', '/', __FILE__)) {
	// You cannot request this file directly.
	exit;
}

function getWebhook($type = "") {
	global $config;
	if(!isset($config['discord'])) return "";
	$key = ($type != "" && isset($config['discord'][$type]))?$type:"webhook";

	return $config['discord'][$key];
}

function discordMessage($message, $webhook_key = "webhook") {
	global $config;
	$webhook = getWebhook($webhook_key);
	if($webhook == "" || !isset($config['discord']['username'])) return;
	$json_data = [
		"content" => $message,
		"username" => $config['discord']['username']
	];
	if(isset($config['discord']['avatar_url']))
		$json_data['avatar_url'] = $config['discord']['avatar_url'];

	$json_req = json_encode($json_data, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

	$ch = curl_init($webhook);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $json_req);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$response = curl_exec($ch);
	curl_close($ch);
	return $response;
}

function discordReport($reason, $postID, $threadID, $board, $ip) {
	if($threadID == "") $threadID = $postID;
	return discordMessage(
		"@here New report:\n" .
		"Reason: $reason\n" .
		"Reported post: https://pchan.co/mod.php?/$board/res/$threadID.html#$postID\n" .
		"Reporter IP: https://pchan.co/mod.php?/IP/$ip\n" .
		"Reports page: https://pchan.co/mod.php?/reports\n",
		"report_webhook"
	);
}

