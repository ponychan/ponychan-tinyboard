<?php

if (realpath($_SERVER['SCRIPT_FILENAME']) == str_replace('\\', '/', __FILE__)) {
	// You cannot request this file directly.
	exit;
}

require_once 'inc/functions.php';

use GeoIp2\Database\Reader;

utf8_clean_userinput();

$userhash = null;
check_userid();

fix_cloudflare_headers();

$timings = array();
$timing_details = array();
timing_mark('start');
register_shutdown_function('timing_end');

register_shutdown_function('fatal_error_handler');
mb_internal_encoding('UTF-8');
loadConfig();

$country_codes = array(
	"ax","al","dz","as","ad","ao","ai","aq","ag","ar","am","aw","au","at",
	"az","bs","bh","bd","bb","by","be","bz","bj","bm","bt","bo","ba","bw",
	"bv","br","io","bn","bg","bf","bi","kh","cm","ca","cv","ky","cf","td",
	"cl","cn","cx","cc","co","km","cg","cd","ck","cr","ci","hr","cu","cy",
	"cz","dk","dj","dm","do","ec","eg","sv","gq","er","ee","et","fk","fo",
	"fj","fi","fr","gf","pf","tf","ga","gm","ge","de","gh","gi","gr","gl",
	"gd","gp","gu","gt","gg","gn","gw","gy","ht","hm","va","hn","hk","hu",
	"is","in","id","ir","iq","ie","im","il","it","jm","jp","je","jo","kz",
	"ke","ki","kr","kw","kg","la","lv","lb","ls","lr","ly","li","lt","lu",
	"mo","mk","mg","mw","my","mv","ml","mt","mh","mq","mr","mu","yt","mx",
	"fm","md","mc","mn","me","ms","ma","mz","mm","na","nr","np","nl","an",
	"nc","nz","ni","ne","ng","nu","nf","mp","no","om","pk","pw","ps","pa",
	"pg","py","pe","ph","pn","pl","pt","pr","qa","re","ro","ru","rw","bl",
	"sh","kn","lc","mf","pm","vc","ws","sm","st","sa","sn","rs","sc","sl",
	"sg","sk","si","sb","so","za","gs","es","lk","sd","sr","sj","sz","se",
	"ch","sy","tw","tj","tz","th","tl","tg","tk","to","tt","tn","tr","tm",
	"tc","tv","ug","ua","ae","gb","us","um","uy","uz","vu","ve","vn","vg",
	"vi","wf","eh","ye","zm","zw"
);

// from https://gist.github.com/vxnick/380904
$country_names = array(
	'af' => 'Afghanistan',
	'ax' => 'Aland Islands',
	'al' => 'Albania',
	'dz' => 'Algeria',
	'as' => 'American Samoa',
	'ad' => 'Andorra',
	'ao' => 'Angola',
	'ai' => 'Anguilla',
	'aq' => 'Antarctica',
	'ag' => 'Antigua And Barbuda',
	'ar' => 'Argentina',
	'am' => 'Armenia',
	'aw' => 'Aruba',
	'au' => 'Australia',
	'at' => 'Austria',
	'az' => 'Azerbaijan',
	'bs' => 'Bahamas',
	'bh' => 'Bahrain',
	'bd' => 'Bangladesh',
	'bb' => 'Barbados',
	'by' => 'Belarus',
	'be' => 'Belgium',
	'bz' => 'Belize',
	'bj' => 'Benin',
	'bm' => 'Bermuda',
	'bt' => 'Bhutan',
	'bo' => 'Bolivia',
	'ba' => 'Bosnia And Herzegovina',
	'bw' => 'Botswana',
	'bv' => 'Bouvet Island',
	'br' => 'Brazil',
	'io' => 'British Indian Ocean Territory',
	'bn' => 'Brunei Darussalam',
	'bg' => 'Bulgaria',
	'bf' => 'Burkina Faso',
	'bi' => 'Burundi',
	'kh' => 'Cambodia',
	'cm' => 'Cameroon',
	'ca' => 'Canada',
	'cv' => 'Cape Verde',
	'ky' => 'Cayman Islands',
	'cf' => 'Central African Republic',
	'td' => 'Chad',
	'cl' => 'Chile',
	'cn' => 'China',
	'cx' => 'Christmas Island',
	'cc' => 'Cocos (Keeling) Islands',
	'co' => 'Colombia',
	'km' => 'Comoros',
	'cg' => 'Congo',
	'cd' => 'Congo, Democratic Republic',
	'ck' => 'Cook Islands',
	'cr' => 'Costa Rica',
	'ci' => 'Cote D\'Ivoire',
	'hr' => 'Croatia',
	'cu' => 'Cuba',
	'cy' => 'Cyprus',
	'cz' => 'Czech Republic',
	'dk' => 'Denmark',
	'dj' => 'Djibouti',
	'dm' => 'Dominica',
	'do' => 'Dominican Republic',
	'ec' => 'Ecuador',
	'eg' => 'Egypt',
	'sv' => 'El Salvador',
	'gq' => 'Equatorial Guinea',
	'er' => 'Eritrea',
	'ee' => 'Estonia',
	'et' => 'Ethiopia',
	'fk' => 'Falkland Islands (Malvinas)',
	'fo' => 'Faroe Islands',
	'fj' => 'Fiji',
	'fi' => 'Finland',
	'fr' => 'France',
	'gf' => 'French Guiana',
	'pf' => 'French Polynesia',
	'tf' => 'French Southern Territories',
	'ga' => 'Gabon',
	'gm' => 'Gambia',
	'ge' => 'Georgia',
	'de' => 'Germany',
	'gh' => 'Ghana',
	'gi' => 'Gibraltar',
	'gr' => 'Greece',
	'gl' => 'Greenland',
	'gd' => 'Grenada',
	'gp' => 'Guadeloupe',
	'gu' => 'Guam',
	'gt' => 'Guatemala',
	'gg' => 'Guernsey',
	'gn' => 'Guinea',
	'gw' => 'Guinea-Bissau',
	'gy' => 'Guyana',
	'ht' => 'Haiti',
	'hm' => 'Heard Island & Mcdonald Islands',
	'va' => 'Holy See (Vatican City State)',
	'hn' => 'Honduras',
	'hk' => 'Hong Kong',
	'hu' => 'Hungary',
	'is' => 'Iceland',
	'in' => 'India',
	'id' => 'Indonesia',
	'ir' => 'Iran, Islamic Republic Of',
	'iq' => 'Iraq',
	'ie' => 'Ireland',
	'im' => 'Isle Of Man',
	'il' => 'Israel',
	'it' => 'Italy',
	'jm' => 'Jamaica',
	'jp' => 'Japan',
	'je' => 'Jersey',
	'jo' => 'Jordan',
	'kz' => 'Kazakhstan',
	'ke' => 'Kenya',
	'ki' => 'Kiribati',
	'kr' => 'Korea',
	'kw' => 'Kuwait',
	'kg' => 'Kyrgyzstan',
	'la' => 'Lao People\'s Democratic Republic',
	'lv' => 'Latvia',
	'lb' => 'Lebanon',
	'ls' => 'Lesotho',
	'lr' => 'Liberia',
	'ly' => 'Libyan Arab Jamahiriya',
	'li' => 'Liechtenstein',
	'lt' => 'Lithuania',
	'lu' => 'Luxembourg',
	'mo' => 'Macao',
	'mk' => 'Macedonia',
	'mg' => 'Madagascar',
	'mw' => 'Malawi',
	'my' => 'Malaysia',
	'mv' => 'Maldives',
	'ml' => 'Mali',
	'mt' => 'Malta',
	'mh' => 'Marshall Islands',
	'mq' => 'Martinique',
	'mr' => 'Mauritania',
	'mu' => 'Mauritius',
	'yt' => 'Mayotte',
	'mx' => 'Mexico',
	'fm' => 'Micronesia, Federated States Of',
	'md' => 'Moldova',
	'mc' => 'Monaco',
	'mn' => 'Mongolia',
	'me' => 'Montenegro',
	'ms' => 'Montserrat',
	'ma' => 'Morocco',
	'mz' => 'Mozambique',
	'mm' => 'Myanmar',
	'na' => 'Namibia',
	'nr' => 'Nauru',
	'np' => 'Nepal',
	'nl' => 'Netherlands',
	'an' => 'Netherlands Antilles',
	'nc' => 'New Caledonia',
	'nz' => 'New Zealand',
	'ni' => 'Nicaragua',
	'ne' => 'Niger',
	'ng' => 'Nigeria',
	'nu' => 'Niue',
	'nf' => 'Norfolk Island',
	'mp' => 'Northern Mariana Islands',
	'no' => 'Norway',
	'om' => 'Oman',
	'pk' => 'Pakistan',
	'pw' => 'Palau',
	'ps' => 'Palestinian Territory, Occupied',
	'pa' => 'Panama',
	'pg' => 'Papua New Guinea',
	'py' => 'Paraguay',
	'pe' => 'Peru',
	'ph' => 'Philippines',
	'pn' => 'Pitcairn',
	'pl' => 'Poland',
	'pt' => 'Portugal',
	'pr' => 'Puerto Rico',
	'qa' => 'Qatar',
	're' => 'Reunion',
	'ro' => 'Romania',
	'ru' => 'Russian Federation',
	'rw' => 'Rwanda',
	'bl' => 'Saint Barthelemy',
	'sh' => 'Saint Helena',
	'kn' => 'Saint Kitts And Nevis',
	'lc' => 'Saint Lucia',
	'mf' => 'Saint Martin',
	'pm' => 'Saint Pierre And Miquelon',
	'vc' => 'Saint Vincent And Grenadines',
	'ws' => 'Samoa',
	'sm' => 'San Marino',
	'st' => 'Sao Tome And Principe',
	'sa' => 'Saudi Arabia',
	'sn' => 'Senegal',
	'rs' => 'Serbia',
	'sc' => 'Seychelles',
	'sl' => 'Sierra Leone',
	'sg' => 'Singapore',
	'sk' => 'Slovakia',
	'si' => 'Slovenia',
	'sb' => 'Solomon Islands',
	'so' => 'Somalia',
	'za' => 'South Africa',
	'gs' => 'South Georgia And Sandwich Isl.',
	'es' => 'Spain',
	'lk' => 'Sri Lanka',
	'sd' => 'Sudan',
	'sr' => 'Suriname',
	'sj' => 'Svalbard And Jan Mayen',
	'sz' => 'Swaziland',
	'se' => 'Sweden',
	'ch' => 'Switzerland',
	'sy' => 'Syrian Arab Republic',
	'tw' => 'Taiwan',
	'tj' => 'Tajikistan',
	'tz' => 'Tanzania',
	'th' => 'Thailand',
	'tl' => 'Timor-Leste',
	'tg' => 'Togo',
	'tk' => 'Tokelau',
	'to' => 'Tonga',
	'tt' => 'Trinidad And Tobago',
	'tn' => 'Tunisia',
	'tr' => 'Turkey',
	'tm' => 'Turkmenistan',
	'tc' => 'Turks And Caicos Islands',
	'tv' => 'Tuvalu',
	'ug' => 'Uganda',
	'ua' => 'Ukraine',
	'ae' => 'United Arab Emirates',
	'gb' => 'United Kingdom',
	'us' => 'United States',
	'um' => 'United States Outlying Islands',
	'uy' => 'Uruguay',
	'uz' => 'Uzbekistan',
	'vu' => 'Vanuatu',
	've' => 'Venezuela',
	'vn' => 'Viet Nam',
	'vg' => 'Virgin Islands, British',
	'vi' => 'Virgin Islands, U.S.',
	'wf' => 'Wallis And Futuna',
	'eh' => 'Western Sahara',
	'ye' => 'Yemen',
	'zm' => 'Zambia',
	'zw' => 'Zimbabwe',
);

function getCountryInfo() {
	global $config;
	global $country_names;
	$ip = $_SERVER['REMOTE_ADDR'];
	$blank_flag = array(
		'country_type' => "blank",
		'country_code' => "blank.gif",
		'country_name' => "No flag"
	);
	$test_flag = array(
		'country_type' => "test",
		'country_code' => "test.png",
		'country_name' => "test country"
	);

	$flag_arr = explode("/", $_POST['post_flag'], 2); // should be of the form "type/flagpath"
	if(count($flag_arr) < 2) {
		return $blank_flag;
	}

	$country_type = $flag_arr[0];
	$country_code = $flag_arr[1];
	$country_name = "";

	switch($country_type) {
		case 'geoip':
			if(isset($_SERVER['HTTP_CF_IPCOUNTRY'])) {
				$country_code = strtolower($_SERVER["HTTP_CF_IPCOUNTRY"]);
				$country_name = $country_names[$country_code];
				return array(
					'country_type' => "geoip",
					'country_code' => $country_code,
					'country_name' => $country_name
				);
			}
			// load the MaxMind GeoLite db, appears to have issues with some IPs
			// since this is the less accurate free version
			$ipreader = new Reader($config['geoip_db']);
			$post_geoip = "";
			try {
				$record = $ipreader->country($ip);
				$post_geoip = strtolower($record->country->isoCode);
				$country_name = $record->country->names['en'];
			} catch(Exception $e) {
				syslog(LOG_ERR, "Error getting country from GeoIP database: $e");
			}
			if($post_geoip == "" || $country_name == "") {
				return $blank_flag;
			}
			$country_code = $post_geoip;
			$country_name = $country_name;
			
			return array(
				'country_type' => "geoip",
				'country_code' => $country_code,
				'country_name' => $country_name
			);

		case 'blank':
			return $blank_flag;

		case 'custom':
			foreach($config['flags'] as $flag) {
				if($flag[1] == $country_code) {
					return array(
						'country_type' => "custom",
						'country_code' => $country_code,
						'country_name' => $flag[0]
					);
				}
			}
			// flag wasn't found
			return $blank_flag;
	}

	return $blank_flag;
}

function getUsableCountryCode($country, $type) {
	global $config;
	global $country_codes;

	if($type == "geoip") {
		if(in_array($country, $country_codes)) {
			return $country;
		}
		return "unknown";
	} else if($type == "custom") {
		foreach($config['flags'] as $flag) {
			if($country == $flag[1]) {
				return $flag[1];
			}
		}
		return "blank.gif";
	}
	return "blank.gif";
}

function isFlagboard($board) {
	global $config;
	if(isset($board) && file_exists($board . '/config.php')) {
		require $board . '/config.php';
	}

	return $config['post_flags'];
}

function hasFlagType($type, $board) {
	global $config;
	if(!isFlagboard($board)) {
		// $config['post_flags'] is false either globally or in the board directory
		return false;
	}

	switch($type) {
		case 'geoip':
			// flags representing the poster's country as detected by the MaxMind db
			return $config['post_geoip'];
		case 'blank':
			// a "No flag" option
			return $config['post_noflag'];
		case 'custom':
			// custom flags as uploaded by the admin
			return count($config['flags']) > 0;
		default:
			// code...
			break;
	}
	return false;
}

function isValidFlag($flag, $type, $board) {
	global $config;
	if(!hasFlagType($type, $board) || !key_exists("flags", $config) || sizeof($config['flags']))
		return false;
	if($type == "geoip") return true;

	foreach ($config['flags'] as $customflag) {
		if ($flag == $customflag[1]) {
			return true;
		}
	}
	return false;
}
