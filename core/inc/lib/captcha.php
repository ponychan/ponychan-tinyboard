<?php

if (realpath($_SERVER['SCRIPT_FILENAME']) == str_replace('\\', '/', __FILE__)) {
	// You cannot request this file directly.
	exit;
}

class CaptchaResult {
	public bool $is_valid;
	public DateTime $timestamp;
	public string $hostname;
	private string $captcha_response;
	public mixed $captcha_json;
	public static function autoAccept() {
		return new CaptchaResult(true, $_SERVER['REMOTE_ADDR']);
	}
	public static function autoReject() {
		return new CaptchaResult(false, $_SERVER['REMOTE_ADDR']);
	}
	public static function fromHcaptchaResponse(string|bool $resp) {
		if(!$resp) {
			return new CaptchaResult(false, $_SERVER['REMOTE_ADDR']);
		}
		$dict = json_decode($resp, true);
		$result = new CaptchaResult($dict['success'], $_SERVER['REMOTE_ADDR']);
		$result->captcha_response = $resp;
		$result->captcha_json = $dict;
		return $result;
	}
	private function __construct(bool $success, string $hostname, DateTime $timestamp = new DateTime()) {
		$this->is_valid = $success;
		$this->timestamp = $timestamp;
		$this->hostname = $hostname;
	}
}


function captchaCheckResult() {
	global $config;
	switch ($config['captcha']) {
		case NO_CAPTCHA:
			return CaptchaResult::autoAccept();
		case USE_HCAPTCHA:
			if(!isset($_POST['h-captcha-response'])) {
				return CaptchaResult::autoReject();
			}
			$data = array(
				'secret' => $config['captcha_private'],
				'response' => $_POST['h-captcha-response']
			);
			$req = curl_init();
			curl_setopt($req, CURLOPT_URL, "https://hcaptcha.com/siteverify");
			curl_setopt($req, CURLOPT_POST, true);
			curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($data));
			curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
			$resp = curl_exec($req);
			return CaptchaResult::fromHcaptchaResponse($resp);
		default:
			error("Invalid configured captcha");
			return CaptchaResult::autoReject();
	}
}