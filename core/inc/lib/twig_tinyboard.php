<?php

require_once 'inc/vendor/autoload.php';

use \Twig\Extension\AbstractExtension;
use \Twig\TwigFunction;
use \Twig\TwigFilter;

class TwigExt_Tinyboard extends AbstractExtension
{
	/**
	* Returns a list of filters to add to the existing list.
	*
	* @return array An array of filters
	*/
	public function getFilters()
	{
		return Array(
			new TwigFilter('filesize', 'format_bytes'),
			new TwigFilter('truncate', 'twig_truncate_filter'),
			new TwigFilter('truncate_body', 'truncate'),
			new TwigFilter('extension', 'twig_extension_filter'),
			new TwigFilter('sprintf', 'sprintf'),
			new TwigFilter('capcode', 'capcode'),
			new TwigFilter('hasFlagType', 'twig_hasFlagType_filter'),
			new TwigFilter('isValidFlag', 'twig_isValidFlag_filter'),
			new TwigFilter('hasPermission', 'twig_hasPermission_filter'),
			new TwigFilter('date', 'twig_date_filter'),
			new TwigFilter('poster_id', 'poster_id'),
			new TwigFilter('remove_whitespace', 'twig_remove_whitespace_filter', ['pre_escape' => 'html', 'is_safe' => ['html']]),
			new TwigFilter('count', 'count'),
			new TwigFilter('ago', 'ago'),
			new TwigFilter('until', 'until'),
			new TwigFilter('time_length', 'time_length'),
			new TwigFilter('push', 'twig_push_filter'),
			new TwigFilter('filemtime', 'filemtime'),
			new TwigFilter('bidi_cleanup', 'bidi_cleanup'),
			new TwigFilter('addslashes', 'addslashes'),
			new TwigFilter('ipToUserRange', 'ipToUserRange'),
			new TwigFilter('mask', 'render_mask'),
			new TwigFilter('mask_url', 'mask_url'),
			new TwigFilter('ban_type_name', 'ban_type_name')
		);
	}

	/**
	* Returns a list of functions to add to the existing list.
	*
	* @return array An array of filters
	*/
	public function getFunctions()
	{
		return Array(
			new TwigFunction('getBoardConfig', 'getBoardConfig'),
			new TwigFunction('time', 'time'),
			new TwigFunction('floor', 'floor'),
			new TwigFunction('timezone', 'twig_timezone_function'),
		);
	}

	/**
	* Returns the name of the extension.
	*
	* @return string The extension name
	*/
	public function getName()
	{
		return 'tinyboard';
	}
}

function twig_timezone_function() {
	return 'Z';
}

function twig_push_filter($array, $value) {
	array_push($array, $value);
	return $array;
}

function twig_remove_whitespace_filter($data) {
	return preg_replace('/[\t\r\n]/', '', $data);
}

function twig_date_filter($date, $format) {
	require_once "inc/config.php";
	global $config;
	$fmt = datefmt_create(
		"en_US",
		IntlDateFormatter::FULL,
		IntlDateFormatter::FULL,
		$config['timezone'],
		IntlDateFormatter::GREGORIAN,
		$format
	);
	return $fmt->format($date);
	// return gmstrftime($format, $date);
}

function twig_hasFlagType_filter($toss, $type, $board) {
	return hasFlagType($type, $board);
}

function twig_isValidFlag_filter($toss, $flag, $type, $board) {
	return isValidFlag($flag, $type, $board);
}

function twig_hasPermission_filter($mod, $permission, $board = null) {
	return hasPermission($permission, $board, $mod);
}

function twig_extension_filter($value, $case_insensitive = true) {
	$ext = substr($value, strrpos($value, '.') + 1);
	if($case_insensitive)
		$ext = strtolower($ext);
	return $ext;
}

function twig_sprintf_filter( $value, $var) {
	return sprintf($value, $var);
}

function twig_truncate_filter($value, $length = 30, $preserve = false, $separator = '…') {
	if (mb_strlen($value) > $length) {
		if ($preserve) {
			if (false !== ($breakpoint = mb_strpos($value, ' ', $length))) {
				$length = $breakpoint;
			}
		}
		return mb_substr($value, 0, $length) . $separator;
	}
	return $value;
}
