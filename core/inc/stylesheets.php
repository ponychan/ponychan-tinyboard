<?php

if (realpath($_SERVER['SCRIPT_FILENAME']) == str_replace('\\', '/', __FILE__)) {
	// You cannot request this file directly.
	exit;
}

// Custom stylesheets available.

// The prefix for each stylesheet URI. Defaults to $config['root']/stylesheets/
// $config['uri_stylesheets'] = 'http://static.example.org/stylesheets/';


// The default stylesheet to use
$config['default_stylesheet'] = 'Cloudsdale';

// Values are array(displayName, file)
// Yeah, the name and displayName match for most right now. This lets us change
// the displayNames in the future without overriding people's settings.
$config['stylesheets']['Cloudsdale'] = array('Cloudsdale', 'Cloudsdale.css');
$config['stylesheets']['Colgate'] = array('Colgate', 'colgate.css');
$config['stylesheets']['Zecora'] = array('Zecora', 'zecora.css');
$config['stylesheets']['Midnight'] = array('Midnight', 'midnight.css');
$config['stylesheets']['Twilight'] = array('Twilight', 'twilight.css');
$config['stylesheets']['Twilight Tackle'] = array('Twilight Tackle', 'twilight-tackle.css');
$config['stylesheets']['Rainbow Dash'] = array('Rainbow Dash', 'rainbow-dash.css');
$config['stylesheets']['Fluttershy'] = array('Fluttershy', 'fluttershy.css');
$config['stylesheets']['Applejack'] = array('Applejack', 'applejack.css');
$config['stylesheets']['Rarity'] = array('Rarity', 'rarity.css');
$config['stylesheets']['Nightmare Moon'] = array('Nightmare Moon', 'nightmare-moon.css');
$config['stylesheets']['Vinyl Scratch'] = array('Vinyl Scratch', 'scratch.css');
$config['stylesheets']['Vinyl Trance'] = array('Vinyl Trance', 'vinyl.css');
$config['stylesheets']['Big Mac'] = array('Big Mac', 'bigmac.css');
$config['stylesheets']['Timelord'] = array('Timelord', 'timelord.css');
$config['stylesheets']['Octavia'] = array('Octavia', 'octavia.css');
$config['stylesheets']['Trixie'] = array('Trixie', 'trixie.css');
$config['stylesheets']['Aloe and Lotus'] = array('Aloe and Lotus', 'aloe-and-lotus.css');
$config['stylesheets']['Cheerilee'] = array('Cheerilee', 'cheerilee.css');
$config['stylesheets']['Scootaloo'] = array('Scootaloo', 'scootaloo.css');
$config['stylesheets']['Daring Do'] = array('Daring Do', 'daring-do.css');
$config['stylesheets']['Royal Guard'] = array('Royal Guard', 'guard.css');
$config['stylesheets']['Gilda'] = array('Gilda', 'gilda.css');
$config['stylesheets']['Discord'] = array('Discord', 'discord.css');
$config['stylesheets']['Crystal Rarity'] = array('Crystal Rarity', 'crystal-rarity.css');

$config['stylesheets']['Pone'] = array('Pone', 'pone.css');
$config['stylesheets']['Yotsuba B'] = array('Yotsuba B', ''); // default
$config['stylesheets']['Yotsuba'] = array('Yotsuba', 'yotsuba.css');
// $config['stylesheets']['Futaba'] = array('Futaba', 'futaba.css');

$config['stylesheets']['Luna (S1)'] = array('Luna (S1)', 'season-one-luna.css');
$config['stylesheets']['Luna (S2)'] = array('Luna (S2)', 'luna.css');
$config['stylesheets']['Wonderbolts'] = array('Wonderbolts', 'wonderbolts.css');
$config['stylesheets']['Shadowbolts'] = array('Shadowbolts', 'shadowbolts.css');
$config['stylesheets']['Nightmare'] = array('Halloween', 'nightmare.css');
$config['stylesheets']['Australian'] = array('Australian', 'Australian.css');
$config['stylesheets']['Chrysalis'] = array('Chrysalis', 'chrysalis.css');
$config['stylesheets']['Changeling'] = array('Changeling', 'changeling.css');
$config['stylesheets']['Tomorrow'] = array('Tomorrow', 'Tomorrow.css');
$config['stylesheets']['Derpy'] = array('Derpy', 'derpy.css');
$config['stylesheets']['Pinkie'] = array('Pinkie', 'pinkie.css');
$config['stylesheets']['Starlight Glimmer'] = array('Starlight Glimmer', 'glimglam.css');
$config['stylesheets']['Geocities'] = array('Geocities', 'geocities.css');
$config['stylesheets']['Photon'] = array('Photon', 'photon.css');
$config['stylesheets']['Windows 98'] = array('Windows 98', 'win98.css');
// $config['stylesheets']['Retro'] = array('Retro', 'retro.css');
