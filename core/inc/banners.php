<?php

if (realpath($_SERVER['SCRIPT_FILENAME']) == str_replace('\\', '/', __FILE__)) {
// You cannot request this file directly.
exit;
}

// Page image banners
$config['banner_prefix'] = 'static/banners/';
// Array is (filename, width, height)
// $config['banners'][] = array('some%20image.png', 600, 150);
// $config['pchan_banners'][] = array('aqua-mikie.jpg', 600, 150);
// $config['pchan_banners'][] = array('triniti-toilet.png', 515, 150);
// $config['pchan_banners'][] = array('pipeschan-mikie.jpg', 600, 150);
$config['pchan_banners'][] = array('pban2-mk17.gif', 600, 150);
// $config['pchan_banners'][] = array('toiletcringe-tracer.jpg', 600, 150);
// $config['pchan_banners'][] = array('wagana-mikie.png', 600, 150);
$config['pchan_banners'][] = array('possibilities-scratch.png', 600, 150);
$config['pchan_banners'][] = array('judgement-mcedge.png', 600, 150);
// $config['pchan_banners'][] = array('pleaforattention-mk17.jpg', 600, 150);
// $config['pchan_banners'][] = array('tworetards-toilet.jpg', 596, 150);
$config['pchan_banners'][] = array('placeofhonor-mcedge.png', 600, 150);
// $config['pchan_banners'][] = array('lovecovereth-toilet.png', 600, 150);
$config['pchan_banners'][] = array('yobuddy-nash.png', 600, 150);
$config['pchan_banners'][] = array('rat-zeke.png', 600, 150);
$config['pchan_banners'][] = array('thisisfine-mk17.jpg', 600, 150);
// $config['pchan_banners'][] = array('tracer.jpg', 600, 150);
$config['pchan_banners'][] = array('pchan-kreamy.png', 600, 150);
// $config['pchan_banners'][] = array('banner2-ika.jpg', 600, 150);
$config['pchan_banners'][] = array('banner-ika.png', 600, 150);
$config['pchan_banners'][] = array('datachan-banner.png', 400, 100);

$config['ponychan_banners'][] = array('agiri-panic.png', 400, 100);
$config['ponychan_banners'][] = array('batman.gif', 400, 100);
//$config['ponychan_banners'][] = array('colshy-cmc.jpg', 400, 100);
// $config['ponychan_banners'][] = array('colshy-copypasta.jpg', 333, 100);
$config['ponychan_banners'][] = array('colshy-fs.jpg', 400, 100);
$config['ponychan_banners'][] = array('colshy-groovy.jpg', 400, 100);
$config['ponychan_banners'][] = array('colshy-humanchan.jpg', 400, 100);
// $config['ponychan_banners'][] = array('colshy-khan.jpg', 400, 100);
// $config['ponychan_banners'][] = array('colshy-no.jpg', 400, 100);
$config['ponychan_banners'][] = array('colshy-the%20ride.jpg', 400, 100);
// $config['ponychan_banners'][] = array('cpu-wrestler.jpg', 400, 100);
$config['ponychan_banners'][] = array('eleanoré-scootachan.png', 400, 100);
// $config['ponychan_banners'][] = array('evamena-rvb.jpg', 400, 100);
// $config['ponychan_banners'][] = array('fen-sweetie.jpg', 400, 100);
// $config['ponychan_banners'][] = array('forever.jpg', 400, 100);
$config['ponychan_banners'][] = array('knight-derpy.jpg', 400, 100);
$config['ponychan_banners'][] = array('knight-logo.jpg', 400, 100);
$config['ponychan_banners'][] = array('knight-mario.gif', 400, 100);
$config['ponychan_banners'][] = array('knight-terminal.gif', 400, 100);
$config['ponychan_banners'][] = array('mikie-eckg.jpg', 400, 100);
// $config['ponychan_banners'][] = array('mudpony.png', 400, 100);
// $config['ponychan_banners'][] = array('pwnies-obey.png', 400, 100);
$config['ponychan_banners'][] = array('salute.png', 400, 100);
$config['ponychan_banners'][] = array('sersys-changeling.gif', 400, 100);
$config['ponychan_banners'][] = array('sersys-lines.png', 400, 100);
$config['ponychan_banners'][] = array('sersys-maud.jpg', 400, 100);
$config['ponychan_banners'][] = array('sersys-menu.gif', 400, 100);
// $config['ponychan_banners'][] = array('sersys-no%20control.gif', 400, 100);
// $config['ponychan_banners'][] = array('sersys-ponies.jpg', 400, 100);
$config['ponychan_banners'][] = array('sersys-sombra.jpg', 400, 100);
$config['ponychan_banners'][] = array('starmane-derails.png', 400, 100);
$config['ponychan_banners'][] = array('tiananmen.jpg', 400, 100);
$config['ponychan_banners'][] = array('wizard-crazy.jpg', 400, 100);
$config['ponychan_banners'][] = array('z-banner.gif', 400, 100);
$config['ponychan_banners'][] = array('z-taco%20tuesday.jpg', 400, 100);
$config['ponychan_banners'][] = array('glimglamcommunism-z.png', 400, 100);
$config['ponychan_banners'][] = array('sweetiebot-z.png', 400, 100);
$config['ponychan_banners'][] = array('vr-z.png', 400, 100);
$config['ponychan_banners'][] = array('wrongway-z.png', 400, 100);
$config['ponychan_banners'][] = array('circles-z.png', 400, 100);
$config['ponychan_banners'][] = array('glimmeranim-z.gif', 400, 100);
$config['ponychan_banners'][] = array('skullchan-wbiy.png', 400, 100);
$config['ponychan_banners'][] = array('glimmer-mikie.png', 400, 100);
$config['ponychan_banners'][] = array('longpony-chewy.png', 400, 100);
$config['ponychan_banners'][] = array('noeggheads-zeke.png', 400, 100);
$config['ponychan_banners'][] = array('regionaldialect-cpbuwI00Vo.png', 400, 100);
$config['ponychan_banners'][] = array('weknowdrama-anon.png', 400, 100);
$config['ponychan_banners'][] = array('stillalive-anon.png', 400, 100);
$config['ponychan_banners'][] = array('oncedaily-cpbuwI00Vo.png', 400, 100);
$config['ponychan_banners'][] = array('glados-whelp.png', 400, 100);
$config['ponychan_banners'][] = array('dramadramadrama-z.gif', 400, 100);
$config['ponychan_banners'][] = array('google-anon.png', 400, 100);
$config['ponychan_banners'][] = array('shitposting-zeke.png', 400, 100);
$config['ponychan_banners'][] = array('server-zeke.gif', 400, 100);
$config['ponychan_banners'][] = array('datachan-banner.png', 400, 100);
