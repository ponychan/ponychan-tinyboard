<?php

/*
 *  Copyright (c) 2010-2013, 2023 Tinyboard Development Group, with changes by Ponychan Developers
 */

if (realpath($_SERVER['SCRIPT_FILENAME']) == str_replace('\\', '/', __FILE__)) {
	// You cannot request this file directly.
	exit;
}

class Cache {
	private static $cache;
	public static function init() {
		global $config;

		switch ($config['cache']['enabled']) {
			case 'memcached':
				self::$cache = new Memcached();
				self::$cache->addServers($config['cache']['memcached']);
				break;
			case 'redis':
				self::$cache = new Redis();
				self::$cache->connect($config['cache']['redis'][0], $config['cache']['redis'][1]);
				if ($config['cache']['redis'][2]) {
					self::$cache->auth($config['cache']['redis'][2]);
				}
				self::$cache->select($config['cache']['redis'][3]) or die('cache select failure');
				break;
			case 'php':
				self::$cache = array();
				break;
		}
	}
	public static function get($key) {
		global $config, $debug;

		$key = $config['cache']['prefix'] . $key;

		$data = false;
		switch ($config['cache']['enabled']) {
			case 'memcached':
				if (!self::$cache)
					self::init();
				$data = self::$cache->get($key);
				break;
			case 'apcu':
				$data = apcu_fetch($key);
				break;
			case 'php':
				$data = isset(self::$cache[$key]) ? self::$cache[$key] : false;
				break;
			case 'redis':
				if (!self::$cache)
					self::init();
				$data = json_decode(self::$cache->get($key), true);
				break;
		}

		// debug
		if ($data !== false && $config['debug']) {
			$debug['cached'][] = $key;
		}

		return $data;
	}
	public static function set($key, $value, $expires = false) {
		global $config;

		$key = $config['cache']['prefix'] . $key;

		if (!$expires)
			$expires = $config['cache']['timeout'];

		if (json_encode($value) === FALSE) {
			error_log('cached value was not jsonifiable!');
			return;
		}

		switch ($config['cache']['enabled']) {
			case 'memcached':
				if (!self::$cache)
					self::init();
				self::$cache->set($key, $value, $expires);
				break;
			case 'redis':
				if (!self::$cache)
					self::init();
				self::$cache->setex($key, $expires, json_encode($value));
				break;
			case 'apcu':
				apcu_store($key, $value, $expires);
				break;
			case 'php':
				self::$cache[$key] = $value;
				break;
		}
	}
	public static function delete($key) {
		global $config;

		$key = $config['cache']['prefix'] . $key;

		switch ($config['cache']['enabled']) {
			case 'memcached':
			case 'redis':
				if (!self::$cache)
					self::init();
				self::$cache->del($key);
				break;
			case 'apcu':
				apcu_delete($key);
				break;
			case 'php':
				unset(self::$cache[$key]);
				break;
		}
	}
	public static function flush() {
		global $config;

		switch ($config['cache']['enabled']) {
			case 'memcached':
				if (!self::$cache)
					self::init();
				return self::$cache->flush();
			case 'apcu':
				return apcu_clear_cache('user');
			case 'php':
				self::$cache = array();
				break;
			case 'redis':
				if (!self::$cache)
					self::init();
				return self::$cache->flushDB();
		}

		return false;
	}
}
