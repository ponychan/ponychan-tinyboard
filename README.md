Ponychan
========
This is the repo for [Ponychan's](https://www.ponychan.net) fork of TinyBoard.

Credit for most of the changes to vanilla TinyBoard go to 
[Macil](https://github.com/Macil). (See [README.old.md](./README.old.md)
for Macil's original README file)

Ponychan's code is split into several different interdependent modules, which are
being moved into their own repositories for better organization.

* __core__ is based on Tinyboard, contains PHP code, and is placed in the
  webserver's publicly accessible document root. When using Vagrant to run the
  site locally, core is run within the Vagrant virtual machine.

* __frontend__ is a complete rewrite of Ponychan's client-core. For (hopefully)
  better organization, it is stored in [a separate repo](https://bitbucket.org/ponychan/ponychan-frontend)
  and linked here as a Git submodule. Because of this, it is not cloned
  along with the rest of the code. To get it, you will need to run
  `git submodule update --init --recursive` if you already have this repo cloned locally, or
  `git clone --recursive https://bitbucket.org/ponychan/ponychan-tinyboard.git`

* __watcher__ contains the server-side code for the /watcher/threads, /mod/filters,
  and /api/* endpoints. It previously used NodeJS but has been rewritten in
  Go. If you don't run this module, then the thread watcher and mod filter editing
  will not work. See [its README](./watcher/README.md) for more info.
  **IMPORTANT:** Now that the watcher is stored in a separate repo, linked here as
  a submodule, it is not cloned along with the rest of the code. To get it, you will
  need to run `git submodule update --init --recursive` if you already have this repo
  cloned locally, or
  `git clone --recursive https://bitbucket.org/ponychan/ponychan-tinyboard.git`

## LICENSE

All of the original Tinyboard code under core/ is under the almost-MIT license
in `core/LICENSE-Tinyboard.md`. The rest of the core code is under the MIT license
in `core/LICENSE-Macil.txt`.

## Quick Development Start

### core

1. Install Vagrant and (recommended but not necessarily required) VirtualBox.
2. Run "vagrant up" to start up a local virtual machine that will run a copy of
 Ponychan on your local computer for development.
3. When that completes, (as it instructs you to) visit
 http://192.168.56.2/install.php to complete the Tinyboard installation,
 while leaving the database settings as default, and then browse to
 http://192.168.56.2/mod.php and log in as admin:password.
4. In the Administration section's "Manage themes" page, click install on the
 "MLPchan" theme and use the default settings. (TODO: Automatically do this in
 the install process.)

All changes to PHP files will immediately take effect in the Vagrant virtual
machine. The one exception is that the virtual machine has its own
`instance-config.php` file. To edit this file directly, you must `vagrant ssh`
to get a shell into the vm, and then run `nano /var/www/instance-config.php`.

Use the following command to see nginx's error log, which will contain
any PHP errors. This is extremely recommended when doing any server work!

    vagrant ssh -- sudo tail --follow=name /var/log/nginx/error.log

There are no PHP unit tests yet. This is an issue.

### frontend

See the __frontend__ bullet point above for info about getting the submodule.
After that:
1. Install Node.js on your system.
2. Make sure you are in the new-frontend branch, by running
 `git checkout new-frontend`.
3. From the ponychan-tinyboard directory, run `cd frontend`. 
4. You may need to run `git submodule update --init --recursive` to populate
 the frontend directory.
5. Run `npm install`
6. Assuming you didn't get any errors (and if you did, see the note below),
 you should be able to run `npx webpack` to transpile the code. If you want to
 have it watch the code for changes and automatically rebuild them, run
 `npx webpack -w` instead.


***Note:***
Depending on your npm version, you may need to run this if you have the most up to date npm version available in your distro's repo but still get an error saying something like "Missing required argument #1" when you run npm install.
```
sudo npm install -g n
sudo n latest
sudo npm install -g npm
hash -d npm
npm install
```

Now run `gulp` to build the client javascript. You may pass the `--watch` (`-w`)
option to make gulp continue running and automatically rebuild the javascript
when any of the source files change.

Use `gulp -m` to make a minified production build. The file "main.js" and a
"maps" directory will be created under core/js. These should be uploaded to the
server.

The javascript codebase was only recently transitioned to Browserify, and a few 
modules still rely on global variables exposed by other modules. The module style 
of the latest Babel installation should be used going further.

### watcher

See [Its README](./watcher/README.md) for info. Unlike previous versions, watcher can be
run inside the virtual machine, as long as it is configured correctly.