#!/usr/bin/env python3

# Don't forget to set flood_time, flood_time_ip, and flood_time_same to 0 in
# instance-config.php before running, otherwise some posts may be rejected
# by the server

import argparse
from os import path
import re
import sys
import unittest
from urllib.parse import urljoin
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.edge.webdriver import WebDriver
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from util.localstorage import LocalStorage
from util.qr import openQR,closeQR,qrIsVisible

testingSite = "http://192.168.56.2"
testingName = "Selenium"
testingEmail = "noko"
testingMessage = "Hello, from Selenium!\n(driver is %s)"
testingSubject = "Selenium post creation"
testingUploadPath = "../core/static/ban.png"
testingPassword = "12345"
testingBoard = "b"

threadRE = re.compile('.*/(\S+)/(\d+)(\+50)?.html')
parser = argparse.ArgumentParser(description="Browser testing via Selenium")
browser = ""

def gotoPage(driver: WebDriver, page: str):
	driver.get(urljoin(testingSite, page))

def loginToStaff(driver: WebDriver, username = "admin", pw = "password"):
	gotoPage(driver, "mod.php")
	driver.find_element(by=By.NAME, value="username").send_keys(username)
	driver.find_element(by=By.NAME, value="password").send_keys(pw)
	driver.find_element(by=By.NAME, value="login").click()

def sendPost(postForm:WebElement, name="", email="", subject="", message="", file=""):
	postForm.find_element(by=By.NAME, value="name").send_keys(name)
	postForm.find_element(by=By.NAME, value="email").send_keys(email)
	postForm.find_element(by=By.NAME, value="subject").send_keys(subject)
	postForm.find_element(by=By.NAME, value="body").send_keys(message)
	if file != "":
		postForm.find_element(by=By.NAME, value="file").send_keys(file)
	postForm.find_element(by=By.NAME, value="post").click()

class TestRunner(unittest.TestCase):
	def setUp(self):
		if browser == "firefox":
			self.driver = webdriver.Firefox()
		elif browser == "chrome" or browser == "chromium":
			self.driver = webdriver.Chrome()
		else:
			self.fail("Unrecognized --browser option %s" % browser)
		return super().setUp()

	def test_qr(self):
		gotoPage(self.driver, testingBoard)
		self.assertIn("/b/ - Beta", self.driver.title)
		elem = self.driver.find_element(by=By.CLASS_NAME, value="subtitle")
		self.assertIn("In development", elem.text)
		openQR(self.driver)
		self.assertTrue(qrIsVisible(self.driver))
		closeQR(self.driver)
		self.assertFalse(qrIsVisible(self.driver))
		openQR(self.driver, False)
		self.assertTrue(qrIsVisible(self.driver))

	def test_localStorage(self):
		gotoPage(self.driver, testingBoard)
		localStorage = LocalStorage(self.driver)
		localStorage["foo"] = "bar"
		self.assertEqual(localStorage.get("foo"), "bar", "making sure that LocalStorage.get works")
		self.assertGreater(localStorage.__len__(), 0)

	def test_login(self):
		loginToStaff(self.driver)
		self.assertEqual(
			self.driver.find_element(by=By.CSS_SELECTOR, value="header h1").text,
			"Dashboard",
			"Testing staff login"
		)

	def test_make_post(self):
		gotoPage(self.driver, testingBoard)
		WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.ID, "qrtogglebox")))

		openQR(self.driver)
		qr = self.driver.find_element(by=By.CSS_SELECTOR, value="div#qr")
		sendPost(
			qr.find_element(by=By.CSS_SELECTOR, value="form"),
			testingName,
			testingEmail,
			testingSubject,
			testingMessage % self.driver.name,
			path.abspath(testingUploadPath)
		)

	def test_mod_delete(self):
		loginToStaff(self.driver)
		gotoPage(self.driver, "/mod.php?/b/")
		WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.ID, "qrtogglebox")))

		openQR(self.driver)
		qr = self.driver.find_element(by=By.CSS_SELECTOR, value="div#qr")
		sendPost(
			qr.find_element(by=By.CSS_SELECTOR, value="form"),
			testingName,
			testingEmail,
			testingSubject,
			testingMessage % self.driver.name,
			path.abspath(testingUploadPath)
		)

		WebDriverWait(self.driver, 10).until(
			EC.url_matches(threadRE))
		threadID = threadRE.findall(self.driver.current_url)[0][1]
		self.driver.find_element(by=By.CSS_SELECTOR, value="div#thread_%s .controls" % threadID).find_element(by=By.LINK_TEXT, value="[D]").click()
		self.driver.switch_to.alert.accept()

	def tearDown(self):
		self.driver.close()
		return super().tearDown()

if __name__ == "__main__":
	testable_browsers = ("firefox","chrome","chromium")
	parser.add_argument("--browser", nargs=1, choices=testable_browsers, required=True)

	args = parser.parse_args()
	browser = args.browser[0]
	sys.argv.pop()
	sys.argv.pop()
	try:
		unittest.main(warnings="ignore")
	except KeyboardInterrupt:
		print("Tests interrupted, exiting")