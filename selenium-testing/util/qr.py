from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.edge.webdriver import WebDriver
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.common.by import By


def qrIsVisible(driver: WebDriver):
	try:
		return driver.find_element(by=By.CSS_SELECTOR, value="div#qr").is_displayed()
	except:
		return False

def enableQR(driver: WebDriver):
	togglebox = driver.find_element(by=By.ID, value="qrtogglebox")
	if togglebox.is_selected() is False:
		togglebox.click()


def disableQR(driver: WebDriver):
	togglebox = driver.find_element(by=By.ID, value="qrtogglebox")
	if togglebox.is_selected():
		togglebox.click()


def openQR(driver: WebDriver, qKey=True):
	enableQR(driver)
	if qrIsVisible(driver):
		return
	togglebox = driver.find_element(by=By.ID, value="qrtogglebox")
	if togglebox.is_selected() is False:
		togglebox.click()
	
	body = driver.find_element(by=By.CSS_SELECTOR, value="body")
	if qKey:
		body.click()
		body.send_keys("q")
	else:
		# click on the "Open the Quick Reply dialog" link to open the QR
		body.find_element(by=By.ID, value="qrDisplayButton").click()

def closeQR(driver: WebDriver):
	if not qrIsVisible(driver):
		return
	closeLink = driver.find_element(by=By.LINK_TEXT, value="X")
	driver.execute_script("arguments[0].click();", closeLink)
