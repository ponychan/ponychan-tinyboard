<?php

/*
 *  Default Vagrant Instance Configuration
 *  --------------------------------------
 *  Edit this file and not config.php for imageboard configuration.
 *
 *  You can copy values from config.php (defaults) and paste them here.
 */



	// Database stuff
	$config['db']['type']		= 'mysql';
	$config['db']['server']		= 'localhost';
	$config['db']['user']		= 'tinyboard';
	$config['db']['password']	= '';
	$config['db']['database']	= 'tinyboard';

	//$config['root']				= '/';

	$config['error_log'] = '/var/log/tinyboard/error.log';
	$config['js_usage_log'] = '/var/log/tinyboard/js_usage.log';
	$config['js_error_log'] = '/var/log/tinyboard/js_error.log';
