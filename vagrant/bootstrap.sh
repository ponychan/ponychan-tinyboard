#!/bin/bash
# Vagrant bootstrap file

set -euo pipefail
export DEBIAN_FRONTEND=noninteractive

rm -f /vagrant/*.log

PHP_VERSION=8.1
GO_VERSION=1.18
NODE_MAJOR=18

add-apt-repository -y ppa:longsleep/golang-backports
apt update -y && apt upgrade -y
apt install -y \
  nginx-extras imagemagick ffmpeg \
  php-common php-igbinary php-imagick php-pear php-redis php-xml \
  php php-pear libmcrypt-dev \
  php-bz2 php-cli php-curl php-fpm php-gd php-json php-mbstring mcrypt php-mysql php${PHP_VERSION}-opcache php-readline \
  redis-server mariadb-server mariadb-client graphicsmagick gifsicle libimage-exiftool-perl mediainfo pandoc \
  libssl-dev php-dev golang-${GO_VERSION} git composer

ln -sf /usr/lib/go-${GO_VERSION}/bin/* /usr/local/bin/


# this is necessary to install the PHP mcrypt extension, since it is no longer provided as
# a precompiled package
pecl channel-update pecl.php.net
printf "\n" | pecl install mcrypt
echo "extension=mcrypt.so" >> /etc/php/${PHP_VERSION}/fpm/php.ini

# install PHP composer libraries
cd /vagrant/core/inc/
sudo -u vagrant composer install

# Install NodeSource keyrings and then install Node
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_${NODE_MAJOR}.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
apt update
apt install nodejs

# Make sure any imported database is utf8mb4
# http://mathiasbynens.be/notes/mysql-utf8mb4
# Put in /etc/mysql/conf.d/local.cnf
cat - <<EOF123 >/etc/mysql/conf.d/local.cnf
[client]
default-character-set = utf8mb4

[mysql]
default-character-set = utf8mb4

[mysqld]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
default-storage-engine = innodb
EOF123

mysql -uroot -e \
"CREATE DATABASE IF NOT EXISTS tinyboard; \
CREATE USER IF NOT EXISTS 'tinyboard'@'localhost' IDENTIFIED BY ''; \
GRANT USAGE ON *.* TO 'tinyboard'@'localhost'; \
GRANT ALL PRIVILEGES ON tinyboard.* TO 'tinyboard'@'localhost'; \
FLUSH PRIVILEGES;"

sed \
  -e 's/post_max_size = .*/post_max_size = 15M/' \
  -e 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' \
  -e 's/upload_max_filesize = .*/upload_max_filesize = 15M/' \
  -i /etc/php/${PHP_VERSION}/fpm/php.ini

#
# vagrant/development specific stuff follows
#

# Make redis open to connections not from localhost
sed \
  -e 's/^bind/#bind/' \
  -i /etc/redis/redis.conf

# Make mysql open to connections not from localhost
cat - <<EOF123 >/etc/mysql/conf.d/open.cnf
[mysqld]
bind-address = 0.0.0.0
EOF123

ln -sf /vagrant/watcher/tinyboard-watcher.service /lib/systemd/system/
if [ ! -f /vagrant/watcher/ponychan-watcher ]; then
  cd /vagrant/watcher
  go build
fi
ln -sf /vagrant/watcher/ponychan-watcher /usr/local/bin/
if [ ! -f /vagrant/watcher/config.json ]; then
  cp /vagrant/watcher/config-example.json /vagrant/watcher/config.json
fi
ln -sf /vagrant/watcher/config.json /etc/tinyboard-watcher.json
systemctl enable redis-server && systemctl restart redis-server &
systemctl enable mysql && systemctl restart mysql &
systemctl enable php${PHP_VERSION}-fpm && systemctl restart php${PHP_VERSION}-fpm &
wait

install -m 775 -o www-data -g www-data -d /var/www
ln -sf \
  /vagrant/core/*.php \
  /vagrant/core/js/ \
  /vagrant/core/static/ \
  /vagrant/core/stylesheets/ \
  /vagrant/core/*.md \
  /vagrant/core/offline-page.html \
  /vagrant/core/LICENSE-Ponychan.txt \
  /vagrant/core/install.sql \
  /var/www/
install -m 775 -o www-data -g www-data -d /var/www/templates
install -m 775 -o www-data -g www-data -d /var/www/templates/cache
ln -sf \
  /vagrant/core/templates/* \
  /var/www/templates/
if ! [ -d /var/www/inc ]; then
  install -m 775 -o www-data -g www-data -d /var/www/inc
  ln -sf \
    /vagrant/core/inc/* \
    /var/www/inc/
  rm -f /var/www/inc/instance-config.php

  # Place default vagrant instance-config.php
  cp /vagrant/vagrant/instance-config.php /var/www/inc/
  chown www-data /var/www/inc/instance-config.php

  # Some default settings for vagrant vm
  ln -s /vagrant/vagrant/site-config.php /var/www/inc/
fi

# VirtualBox shared folders don't play nicely with sendfile.
sed \
  -e 's/sendfile on;/sendfile off;/' \
  -i /etc/nginx/nginx.conf

openssl req -x509 -nodes -days 7305 -newkey rsa:2048 \
	-keyout /etc/ssl/private/nginx-selfsigned.key \
	-out /etc/ssl/certs/nginx-selfsigned.crt \
	-subj "/CN=192.168.56.2"

rm -f /etc/nginx/sites-enabled/* /etc/nginx/sites-available/tinyboard.nginx
# tinyboard.nginx can't be a symlink into /vagrant because nginx starts before
# /vagrant is mounted.
cp /vagrant/vagrant/tinyboard.nginx /etc/nginx/sites-available/
ln -sf /etc/nginx/sites-available/tinyboard.nginx /etc/nginx/sites-enabled/
systemctl disable apache2 && systemctl stop apache2
systemctl enable nginx && systemctl restart nginx

# Create a symbolic link to make the installed Composer libraries usable
ln -sf /vagrant/core/inc/vendor /var/www/inc/vendor

echo
echo "Server set up, please browse to http://192.168.56.2/install.php"
echo "to complete the installation. Default database settings will work."
echo "After you complete the installation steps, go to "
echo "http://192.168.56.2/mod.php and log in as admin:password."
echo "Then follow the instructions in the frontend section of the"
echo "README to set up JavaScript building."
# after everything is done, it should (hypothetically) be safe to run these unless you need Apache
# Doing this isn't necessary, but it (probably) saves some space
# sudo apt remove apache2 apache2-bin apache2-data apache2-utils libapache2-mod-php${PHP_VERSION}
# sudo rm -rf /var/lib/apache2/ /etc/php/${PHP_VERSION}/apache2/
# sudo apt autoremove